package co.com.bvc.services;

import java.util.Date;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@AllArgsConstructor
@NoArgsConstructor
@Data
public class EventoDTO {
	
	private String plataforma;
	
	private String idEvento;
	
	private int numeroEventos;
	
	private Date fechaEvento;
	
	private float costoEvento;
	
}
